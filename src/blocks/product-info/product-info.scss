.s-product-info {
  padding: 1rem;

  &__inner {
    @include section-inner;
  }

  &__certificates,
  &__content,
  &__spec {
    transition-duration: 0.75s;
    transition-timing-function: $transition-timing;
  }

  &__certificates {
    padding: 1rem 1rem 0 1rem;
    transition-delay: 1.25s;

    @media only screen and (min-width: $screen-sm) {
      padding: 5rem 1.5rem 0 1.5rem;
    }
  }

  &__certificates-item {
    line-height: normal;
    margin-bottom: 0.5rem;
  }

  &__certificates-link {
    text-transform: uppercase;
    font-size: ms(-1);
    line-height: 1.3em;
    letter-spacing: 0.05em;
    font-weight: 500;
    position: relative;
    padding-bottom: 0.25rem;
    cursor: pointer;

    &::after {
      content: '';
      position: absolute;
      bottom: 0;
      left: 0;
      height: 1px;
      width: 100%;
      background-color: currentColor;
      transform: scaleX(1);
      transition: transform 0.5s $transition-timing;
      transform-origin: left center;
    }

    &:hover,
    &:focus {

      &::after {
        transform: scaleX(0);
        transform-origin: right center;
      }
    }
  }

  &__content-wrap {

    @media only screen and (min-width: $screen-sm) {
      position: relative;

      &::before {
        position: absolute;
        content: '';
        top: 0;
        left: 0;
        width: 100%;
        height: 1px;
        background-color: $text-color;
        transform: scaleX(1);
        transform-origin: left center;
        transition: $transition-duration $transition-timing;
        will-change: transform;
      }
    }
  }

  &__content {
    padding: 1.5rem 1rem 0 1rem;
    transition-duration: 0.75s;
    transition-timing-function: $transition-timing;
    transition-delay: 1.3s;

    @media only screen and (min-width: $screen-sm) {
      padding: 4.5rem 1.5rem 0 1.5rem;
    }
  }

  &__subtitle {
    font-weight: 500;
    font-size: ms(1);
    line-height: 1.2em;
    margin-top: 0;
    margin-bottom: 0.75rem;

    @media only screen and (min-width: $screen-sm) {
      line-height: 1.3em;
    }
  }

  &__text {
    margin-bottom: 1.5rem;

    p {
      font-size: ms(-1);
      line-height: 1.5em;
      margin-top: 0.75rem;
    }
  }

  &__spec {
    padding: 0 1rem;
    transition-delay: 1.35s;

    @media only screen and (min-width: $screen-sm) {
      padding: 5rem 1.5rem 0 1.5rem;
    }
  }

  &__spec-list {
    margin: 0;
    margin-bottom: 0.75rem;
    font-size: ms(-1);
    line-height: 1.3em;
  }

  &__spec-group {
    margin-bottom: 0.5rem;
    display: flex;
    flex-flow: row wrap;
  }

  &__spec-term {
    font-weight: 500;
  }

  &__spec-desc {
    font-weight: 400;
    font-feature-settings: 'lnum'1;
    margin-left: 0.25rem;

    @supports (font-variant: lining-nums) {
      font-feature-settings: normal;
      font-variant-numeric: lining-nums;
      font-kerning: normal;
    }
  }

  &--is-hidden {

    .js & {

      .s-product-info__certificates,
      .s-product-info__content,
      .s-product-info__spec {
        transform: $transform-from-hidden;
        opacity: 0;
      }

      .s-product-info__content-wrap {

        &::before {
          transform: scaleX(0);
        }
      }
    }
  }
}